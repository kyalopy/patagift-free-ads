from django.contrib import admin
from pata_gift.models import Trader, Product, ProductCategory, ProductImage, ProductLocation


class TraderAdmin(admin.ModelAdmin):
    list_display = ('trader_phone_number', 'trader_website')


class ProductAdmin(admin.ModelAdmin):
    list_display = ('product_trader', 'product_category', 'product_name', 'product_image', 'product_price',
                    'product_quantity', )


class ProductCategoryAdmin(admin.ModelAdmin):
    list_display = ('product_category', )


class ProductLocationAdmin(admin.ModelAdmin):
    list_display = ('product_details', 'trader_main_city', 'trader_sub_area', 'trader_street',
                    'trader_physical_location')


class ProductImageAdmin(admin.ModelAdmin):
    list_display = ('product_name', 'product_image_1', 'product_image_2', 'product_image_3',
                    'product_image_4', 'product_image_5')


admin.site.register(Trader, TraderAdmin)
admin.site.register(ProductCategory, ProductCategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(ProductLocation, ProductLocationAdmin)
admin.site.register(ProductImage, ProductImageAdmin)