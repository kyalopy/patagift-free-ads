
from django.shortcuts import render, get_object_or_404, render_to_response
from django.http import HttpResponseRedirect
from pata_gift.forms import *
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.contrib import messages
from django.core.urlresolvers import reverse


def register(request):
    
    if request.method == 'POST':
        user_form = UserForm(request.POST)
        trader_form = TraderForm(request.POST, request.FILES)
        if User.objects.filter(email=request.POST['email']).exists():
            messages.warning(request, 'Sorry, the email address you have typed is already in use')
            return render(request, 'pata_gift/register.html', {'user_form': user_form,
                                                               'trader_form': trader_form, })
        if Trader.objects.filter(trader_phone_number=request.POST['trader_phone_number']).exists():
            messages.warning(request, 'Sorry, the phone number you have typed is already in use')
            return render(request, 'pata_gift/register.html', {'user_form': user_form,
                                                               'trader_form': trader_form, })
        password1 = request.POST['password']
        password2 = request.POST['password_confirmation']
        if password1 != password2:
            password_error = "Password didn't match"
            return render(request, 'pata_gift/register.html', {'user_form': user_form,
                                                               'trader_form': trader_form,
                                                               'password_error': password_error})

        if user_form.is_valid() and trader_form.is_valid():
            user = user_form.save(commit=False)
            user.set_password(user.password)
            user.save()

            worker = trader_form.save(commit=False)
            worker.user = user
            worker.save()

            user_form = UserForm()
            trader_form = TraderForm()
            messages.info(request, 'Congratulations! your account has been created and activated')

            return HttpResponseRedirect(reverse('register'), {'user_form': user_form,
                                                              'trader_form': trader_form})
        else:
            user_form = UserForm(request.POST)
            trader_form = TraderForm(request.POST)
            return render(request, 'pata_gift/register.html', {'user_form': user_form,
                                                               'trader_form': trader_form})
    else:
        user_form = UserForm()
        trader_form = TraderForm()

        return render(request, 'pata_gift/register.html', {'user_form': user_form,
                                                           'trader_form': trader_form})


def user_login(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)

                return HttpResponseRedirect('/')
            else:
                error_msg = "Your account is not active. Kindly contact " \
                            "the system admin: admin@patagift.com"
                return render(request, 'pata_gift/login.html', {'error_msg': error_msg, })

        else:
            invalid_details = "You entered INVALID credentials. Kindly try again"

            return render(request, 'pata_gift/login.html', {'invalid_details': invalid_details, })

    else:
        return render(request, 'pata_gift/login.html', {})


@login_required
def user_logout(request):
    logout(request)

    return HttpResponseRedirect('/')


def my_account(request):
    category_column = ProductCategory.objects.all()[:3]
    product_trader = Trader.objects.all()
    wedding_gifts = ProductCategory.objects.get(pk=5)
    love_gifts = ProductCategory.objects.get(pk=6)
    birthday_gifts = ProductCategory.objects.get(pk=7)
    anniversary_gifts = ProductCategory.objects.get(pk=8)
    graduation_gifts = ProductCategory.objects.get(pk=9)

    wedding_category = ProductCategory.objects.get(pk=5)
    love_category = ProductCategory.objects.get(pk=6)
    birthday_category = ProductCategory.objects.get(pk=7)
    anniversary_category = ProductCategory.objects.get(pk=8)
    graduation_category = ProductCategory.objects.get(pk=9)
    try:
        trader = Trader.objects.get(user=request.user)
    except Trader.DoesNotExist:
        trader = None
    my_ads = Product.objects.filter(product_trader_id=trader.id).count()
    if trader:
        ads = Product.objects.filter(product_trader_id=trader.id)
        latest_ad = Product.objects.filter(product_trader_id=trader.id).first()
        if latest_ad:
            pass
        else:

            return render(request, 'pata_gift/my-account.html', {'ads': ads,
                                                                 'wedding_category': wedding_category,
                                                                 'love_category': love_category,
                                                                 'birthday_category': birthday_category,
                                                                 'anniversary_category': anniversary_category,
                                                                 'graduation_category': graduation_category,
                                                                 'my_ads': my_ads,
                                                                 'love_gifts': love_gifts,
                                                                 'wedding_gifts': wedding_gifts,
                                                                 'birthday_gifts': birthday_gifts,
                                                                 'anniversary_gifts': anniversary_gifts,
                                                                 'graduation_gifts': graduation_gifts}, )
    else:
        return HttpResponseRedirect('login')

    return render(request, 'pata_gift/my-account.html', {'ads': ads, 'latest': latest_ad,
                                                         'wedding_category': wedding_category,
                                                         'love_category': love_category,
                                                         'birthday_category': birthday_category,
                                                         'anniversary_category': anniversary_category,
                                                         'graduation_category': graduation_category,
                                                         'my_ads': my_ads,
                                                         'love_gifts': love_gifts,
                                                         'wedding_gifts': wedding_gifts,
                                                         'birthday_gifts': birthday_gifts,
                                                         'anniversary_gifts': anniversary_gifts,
                                                         'graduation_gifts': graduation_gifts}, )


@login_required
def create_product_step_1(request, category_id):
    categories = ProductCategory.objects.all()
    category = get_object_or_404(ProductCategory, pk=category_id)

    if request.method == 'POST':
        product_form = ProductForm(request.POST, request.FILES)

        if product_form.is_valid():
            product = product_form.save(commit=False)
            category = ProductCategory.objects.get(product_category=category)
            trader = Trader.objects.get(user=request.user)
            product.product_trader = trader
            product.product_category_id = category.id
            if 'product_image' in request.FILES:
                product.product_image = request.FILES['product_image']
                product.save()
            else:
                messages.warning(request, 'Please choose an image file')
                return render(request, 'pata_gift/create-product-step-1.html', {'product_form': product_form})
            return HttpResponseRedirect(reverse('my_account'))
        else:
            print(product_form.errors)
            return render(request, 'pata_gift/create-product-step-1.html', {'product_form': product_form})
    else:
        product_form = ProductForm()

        return render(request, 'pata_gift/create-product-step-1.html', {'product_form': product_form,
                                                                        'categories': categories})


def create_product_step_2(request, product_id):
    product = get_object_or_404(Product, pk=product_id)
    if product.location_state == 1:
        return HttpResponseRedirect(reverse('my_account'),)

    if request.method == 'POST':
        product_location_form = ProductLocationForm(request.POST)

        if product_location_form.is_valid():
            product_location = product_location_form.save(commit=False)
            product_location.product_details = product
            product_location.save()
            product.location_state = 1
            product.save()

            return HttpResponseRedirect(reverse('my_account'))
        else:
            return render(request, 'pata_gift/create-product-step-2.html',
                          {'product_location_form': product_location_form})
    else:
        product_location_form = ProductLocationForm()
        return render(request, 'pata_gift/create-product-step-2.html',
                      {'product_location_form': product_location_form})


def create_product_step_3(request, product_id):

    product = get_object_or_404(Product, pk=product_id)
    if product.image_state == 1:
        return HttpResponseRedirect(reverse('my_account'),)
    if product.location_state == 0:
        return HttpResponseRedirect(reverse('my_account'),)

    if request.method == 'POST':
        product_images_form = ProductImageForm(request.POST, request.FILES)
        if product_images_form.is_valid():
            product_images = product_images_form.save(commit=False)
            product_images.product_name = product
            product_images.save()
            product.image_state = 1
            product.save()

            return HttpResponseRedirect(reverse('my_account'),)
        else:
            return render(request, 'pata_gift/create-product-step-3.html',
                          {'product_images_form': product_images_form})
    else:
        product_images_form = ProductImageForm()
        return render(request, 'pata_gift/create-product-step-3.html',
                      {'product_images_form': product_images_form})


def reg_form_error(request):
    return render(request, 'pata_gift/reg_form_error.html')


def product_details(request, product_id):
    product = Product.objects.get(pk=product_id)
    product_image1 = ProductImage.objects.filter(product_name_id=product.id)
    product_location = ProductLocation.objects.filter(product_details_id=product.id)
    product_category = ProductCategory.objects.all()
    category_column = ProductCategory.objects.all()[:3]
    product_trader = Trader.objects.all()
    wedding_gifts = ProductCategory.objects.get(pk=5)
    love_gifts = ProductCategory.objects.get(pk=6)
    birthday_gifts = ProductCategory.objects.get(pk=7)
    anniversary_gifts = ProductCategory.objects.get(pk=8)
    graduation_gifts = ProductCategory.objects.get(pk=9)

    context_dic = {
        'product_name': product,
        'product_images': product_image1,
        'product_location': product_location,
        'product_categories': product_category,
        'category_columns': category_column,
        'traders': product_trader,
        'wedding_gifts': wedding_gifts,
        'love_gifts': love_gifts,
        'birthday_gifts': birthday_gifts,
        'anniversary_gifts': anniversary_gifts,
        'graduation_gifts': graduation_gifts

    }

    return render(request, 'pata_gift/product_details.html', context_dic, )


def wedding_gift(request):

    product_category = ProductCategory.objects.all()
    category_column = ProductCategory.objects.all()[:3]

    product_trader = Trader.objects.all()
    wedding_gifts = ProductCategory.objects.get(pk=5)
    love_gifts = ProductCategory.objects.get(pk=6)
    birthday_gifts = ProductCategory.objects.get(pk=7)
    anniversary_gifts = ProductCategory.objects.get(pk=8)
    graduation_gifts = ProductCategory.objects.get(pk=9)

    context_dic = {


        'product_categories': product_category,
        'category_columns': category_column,
        'traders': product_trader,
        'wedding_gifts': wedding_gifts,
        'love_gifts': love_gifts,
        'birthday_gifts': birthday_gifts,
        'anniversary_gifts': anniversary_gifts,
        'graduation_gifts': graduation_gifts

    }

    return render(request, 'pata_gift/wedding.html', context_dic)


def love_gift(request):
    product_category = ProductCategory.objects.all()
    category_column = ProductCategory.objects.all()[:3]

    product_trader = Trader.objects.all()
    wedding_gifts = ProductCategory.objects.get(pk=5)
    love_gifts = ProductCategory.objects.get(pk=6)
    birthday_gifts = ProductCategory.objects.get(pk=7)
    anniversary_gifts = ProductCategory.objects.get(pk=8)
    graduation_gifts = ProductCategory.objects.get(pk=9)

    context_dic = {

        'product_categories': product_category,
        'category_columns': category_column,
        'traders': product_trader,
        'wedding_gifts': wedding_gifts,
        'love_gifts': love_gifts,
        'birthday_gifts': birthday_gifts,
        'anniversary_gifts': anniversary_gifts,
        'graduation_gifts': graduation_gifts

    }
    return render(request, 'pata_gift/love.html', context_dic)


def birthday_gift(request):

    product_category = ProductCategory.objects.all()
    category_column = ProductCategory.objects.all()[:3]

    product_trader = Trader.objects.all()
    wedding_gifts = ProductCategory.objects.get(pk=5)
    love_gifts = ProductCategory.objects.get(pk=6)
    birthday_gifts = ProductCategory.objects.get(pk=7)
    anniversary_gifts = ProductCategory.objects.get(pk=8)
    graduation_gifts = ProductCategory.objects.get(pk=9)

    context_dic = {


        'product_categories': product_category,
        'category_columns': category_column,
        'traders': product_trader,
        'wedding_gifts': wedding_gifts,
        'love_gifts': love_gifts,
        'birthday_gifts': birthday_gifts,
        'anniversary_gifts': anniversary_gifts,
        'graduation_gifts': graduation_gifts

    }
    return render(request, 'pata_gift/birthday.html', context_dic)


def anniversary_gift(request):
    product_category = ProductCategory.objects.all()
    category_column = ProductCategory.objects.all()[:3]

    product_trader = Trader.objects.all()
    wedding_gifts = ProductCategory.objects.get(pk=5)
    love_gifts = ProductCategory.objects.get(pk=6)
    birthday_gifts = ProductCategory.objects.get(pk=7)
    anniversary_gifts = ProductCategory.objects.get(pk=8)
    graduation_gifts = ProductCategory.objects.get(pk=9)

    context_dic = {


        'product_categories': product_category,
        'category_columns': category_column,
        'traders': product_trader,
        'wedding_gifts': wedding_gifts,
        'love_gifts': love_gifts,
        'birthday_gifts': birthday_gifts,
        'anniversary_gifts': anniversary_gifts,
        'graduation_gifts': graduation_gifts

    }
    return render(request, 'pata_gift/anniversary.html', context_dic)


def graduation_gift(request):
    product_category = ProductCategory.objects.all()
    category_column = ProductCategory.objects.all()[:3]

    product_trader = Trader.objects.all()
    wedding_gifts = ProductCategory.objects.get(pk=5)
    love_gifts = ProductCategory.objects.get(pk=6)
    birthday_gifts = ProductCategory.objects.get(pk=7)
    anniversary_gifts = ProductCategory.objects.get(pk=8)
    graduation_gifts = ProductCategory.objects.get(pk=9)

    context_dic = {


        'product_categories': product_category,
        'category_columns': category_column,
        'traders': product_trader,
        'wedding_gifts': wedding_gifts,
        'love_gifts': love_gifts,
        'birthday_gifts': birthday_gifts,
        'anniversary_gifts': anniversary_gifts,
        'graduation_gifts': graduation_gifts

    }
    return render(request, 'pata_gift/graduation.html', context_dic)


def edit_product(request, product_id):
    product = get_object_or_404(Product, pk=product_id)
    if request.method == 'POST':
        product_form = ProductForm(request.POST, instance=product)

        if product_form.is_valid():
            product.edited_state = 1
            product.save()

            return HttpResponseRedirect(reverse('my_account'))
        else:
            product_form = ProductForm(request.POST, instance=product)
            print(product_form.errors)
            return render(request, 'pata_gift/edit-product.html', {'product_form': product_form})
    else:
        product_form = ProductForm(instance=product)
        return render(request, 'pata_gift/edit-product.html', {'product_form': product_form})


def delete_product(request, product_id):
    product = get_object_or_404(Product, pk=product_id)

    if request.method == 'POST':
        product.delete()

        return HttpResponseRedirect(reverse('my_account'))
    return render(request, 'pata_gift/delete-product.html', {'product': product})

def edit_product_location(request, product_id):
    product = get_object_or_404(Product, pk=product_id)
    # product_location = ProductLocation.objects.get(product_details_id=product.id)
    if request.method == 'POST':
        product_location_form = ProductLocationForm(request.POST, instance=product)

        if product_location_form.is_valid():
            product_location = product_location_form.save(commit=False)
            product_location.product_details_id = product

            product.location_state = 1
            product_location.save()

            return HttpResponseRedirect(reverse('my_account'))
        else:
            product_location_form = ProductLocationForm(request.POST, instance=product)
            print(product_location_form.errors)
            return render(request, 'pata_gift/edit-product-location.html',
                          {'product_location_form': product_location_form})
    else:
        product_location_form = ProductLocationForm(instance=product)
        return render(request, 'pata_gift/edit-product-location.html', {'product_location_form': product_location_form})


def search_product(request):
    if request.method == 'POST':
        search_text = request.POST['search_text']
    else:
        search_text = ''

    products = Product.objects.filter(product_name__contains=search_text)
    return render_to_response('ajax_product_search.html', {'products': products})


def edit_profile(request, ):
    user = Trader.objects.get(user=request.user)
    if request.method == 'POST':
        trader_form = TraderForm(request.POST, instance=user)

        if trader_form.is_valid():
            trader_form.save()

            return HttpResponseRedirect(reverse('my_account'))
        else:
            print(trader_form.errors)
            return render(request, 'pata_gift/edit-profile.html', {'trader_form': trader_form})
    else:
        trader_form = TraderForm(instance=user)
        return render(request, 'pata_gift/edit-profile.html', {'trader_form': trader_form})


def profile_preview(request):
    trader = Trader.objects.get(user=request.user)

    return render(request, 'pata_gift/profile-preview.html', {'trader': trader})
