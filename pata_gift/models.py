from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from django.contrib.auth.models import User
from extended_choices import Choices


class Trader(models.Model):
    user = models.OneToOneField(User)
    trader_phone_number = PhoneNumberField()
    trader_website = models.URLField(blank=True)

    def __str__(self):
        return self.user.first_name


class ProductCategory(models.Model):
    product_category = models.CharField(blank=False, max_length=128)

    def __str__(self):
        return self.product_category


class Product(models.Model):

    LOCATION_STATES = Choices(
        ('LOCATION_NOT_ADDED', 0, 'Location not added'),
        ('LOCATION_ADDED', 1, 'Location added')
    )

    IMAGES_STATES = Choices(
        ('IMAGES_NOT_ADDED', 0, 'Images not added'),
        ('IMAGES_ADDED', 1, 'Images added')
    )
    EDIT_STATES = Choices(
        ('NOT_EDITED', 0, 'Not edited'),
        ('EDITED_ONCE', 1, 'Edited once'),
        ('EDITED_TWICE', 2, 'Edited Twice'),
        ('EDITED_THRICE', 3, 'Edited Thrice'),
    )

    product_trader = models.ForeignKey(Trader)
    product_category = models.ForeignKey(ProductCategory)
    product_name = models.CharField(max_length=128, blank=False)
    product_price = models.IntegerField(default=0)
    product_image = models.ImageField()
    product_quantity = models.IntegerField()
    date_posted = models.DateTimeField(auto_now_add=True)
    location_state = models.SmallIntegerField(choices=LOCATION_STATES, default=LOCATION_STATES.LOCATION_NOT_ADDED)
    image_state = models.SmallIntegerField(choices=IMAGES_STATES, default=IMAGES_STATES.IMAGES_NOT_ADDED)
    edited_state = models.SmallIntegerField(choices=EDIT_STATES, default=EDIT_STATES.NOT_EDITED)

    def __str__(self):
        return self.product_name

    class Meta:
        ordering = ['-date_posted']


class ProductImage(models.Model):
    EDIT_STATES = Choices(
        ('NOT_EDITED', 0, 'Not edited'),
        ('EDITED_ONCE', 1, 'Edited once'),
        ('EDITED_TWICE', 2, 'Edited Twice'),
        ('EDITED_THRICE', 3, 'Edited Thrice'),
    )
    product_name = models.ForeignKey(Product)
    product_image_1 = models.ImageField(blank=False)
    product_image_2 = models.ImageField(blank=True, null=True)
    product_image_3 = models.ImageField(blank=True, null=True)
    product_image_4 = models.ImageField(blank=True, null=True)
    product_image_5 = models.ImageField(blank=True, null=True)
    edited_state = models.SmallIntegerField(choices=EDIT_STATES, default=EDIT_STATES.NOT_EDITED)

    def __str__(self):
        return self.product_name.product_name


class ProductLocation(models.Model):
    EDIT_STATES = Choices(
        ('NOT_EDITED', 0, 'Not edited'),
        ('EDITED_ONCE', 1, 'Edited once'),
        ('EDITED_TWICE', 2, 'Edited Twice'),
        ('EDITED_THRICE', 3, 'Edited Thrice'),)

    product_details = models.ForeignKey(Product)
    trader_main_city = models.CharField(max_length=128, blank=False)
    trader_sub_area = models.CharField(max_length=128, blank=False)
    trader_street = models.CharField(max_length=128, blank=False)
    trader_physical_location = models.CharField(max_length=128, blank=False)
    edited_state = models.SmallIntegerField(choices=EDIT_STATES, default=EDIT_STATES.NOT_EDITED)

    def __str__(self):
        return self.trader_main_city
