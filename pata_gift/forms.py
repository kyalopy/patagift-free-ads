from django import forms
from phonenumber_field.modelfields import PhoneNumberField
from pata_gift.models import *


class UserForm(forms.ModelForm):
    first_name = forms.CharField(label='First Name', max_length=30, min_length=2, widget=forms.TextInput(),
                                 error_messages={'required': 'The first name is required'})
    last_name = forms.CharField(label='Last Name', max_length=30, min_length=2, widget=forms.TextInput(),
                                error_messages={'required': 'The last name is required'})
    username = forms.CharField(label='Username', max_length=30, min_length=3, widget=forms.TextInput())
    email = forms.EmailField(label='Email', widget=forms.EmailInput(),)
    password = forms.CharField(widget=forms.PasswordInput(), label='Type password', min_length=6)
    password_confirmation = forms.CharField(widget=forms.PasswordInput(),  min_length=6,
                                            label='Confirm Password')

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'password', 'password_confirmation')


class TraderForm(forms.ModelForm):
    trader_phone_number = PhoneNumberField()
    trader_website = forms.URLField()

    class Meta:
        model = Trader
        fields = ('trader_phone_number', 'trader_website')
        exclude = ['user']

class ProductForm(forms.ModelForm):
    class MyModelChoiceField(forms.ModelChoiceField):
        def clean(self, value):
            return value

        def label_from_instance(self, obj):
            return obj.id

    product_category = MyModelChoiceField(queryset=ProductCategory.objects.all(), label='Item Category')
    product_trader = MyModelChoiceField(queryset=Trader.objects.all(), label="Trader")
    product_name = forms.CharField(max_length=20, required=True, label='Item name')
    product_price = forms.IntegerField(required=False, label='Price')
    product_image = forms.ImageField(label='Image', )
    product_quantity = forms.IntegerField(required=False, label='Quantity')

    class Meta:
        model = Product
        fields = ('product_name', 'product_price', 'product_image',
                  'product_image', 'product_quantity')
        exclude = ('product_trader',)


class ProductLocationForm(forms.ModelForm):
    class MyModelChoiceField(forms.ModelChoiceField):
        def clean(self, value):
            return value

        def label_from_instance(self, obj):
            return obj.id
    product_name = MyModelChoiceField(queryset=Product.objects.all())
    trader_main_city = forms.CharField(max_length=15, label='Main city')
    trader_sub_area = forms.CharField(max_length=15, label='Sub area')
    trader_street = forms.CharField(max_length=15, label='Street')
    trader_physical_location = forms.CharField(max_length=15, label='Physical location')

    class Meta:
        model = ProductLocation
        fields = ('trader_main_city', 'trader_sub_area', 'trader_street', 'trader_physical_location', )
        exclude = ['product_name']


class ProductImageForm(forms.ModelForm):
    class MyModelChoiceField(forms.ModelChoiceField):
        def clean(self, value):
            return value

        def label_from_instance(self, obj):
            return obj.id
    product_name = MyModelChoiceField(queryset=Product.objects.all())
    product_image_1 = forms.ImageField(required=True, label='1')
    product_image_2 = forms.ImageField(required=False, label='2')
    product_image_3 = forms.ImageField(required=False, label='3')
    product_image_4 = forms.ImageField(required=False, label='4')
    product_image_5 = forms.ImageField(required=False, label='5')

    class Meta:
        model = ProductImage
        fields = ('product_image_1', 'product_image_2', 'product_image_3', 'product_image_4', 'product_image_5')
        exclude = ['product_name']
