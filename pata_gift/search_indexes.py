__author__ = 'joan ndegwa'

import datetime
from haystack import indexes
from pata_gift.models import Product


class ProductIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    trader = indexes.CharField(model_attr='product_trader')
    category = indexes.CharField(model_attr='product_category')
    name = indexes.CharField(model_attr='product_name', )
    price = indexes.IntegerField(model_attr='product_price')
    pub_date = indexes.DateTimeField(model_attr='date_posted')

    def get_model(self):
        return Product

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(pub_date__lte=datetime.datetime.now())

