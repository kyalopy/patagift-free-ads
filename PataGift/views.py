from django.shortcuts import render
from pata_gift.models import ProductCategory, Trader


def index(request):
    product_category = ProductCategory.objects.all()
    category_column = ProductCategory.objects.all()[:3]
    product_trader = Trader.objects.all()
    wedding_gifts = ProductCategory.objects.get(pk=5)
    love_gifts = ProductCategory.objects.get(pk=6)
    birthday_gifts = ProductCategory.objects.get(pk=7)
    anniversary_gifts = ProductCategory.objects.get(pk=8)
    graduation_gifts = ProductCategory.objects.get(pk=9)

    context_dic = {'product_categories': product_category,
                   'category_columns': category_column,
                   'traders': product_trader,
                   'wedding_gifts': wedding_gifts,
                   'love_gifts': love_gifts,
                   'birthday_gifts': birthday_gifts,
                   'anniversary_gifts': anniversary_gifts,
                   'graduation_gifts': graduation_gifts
                   }

    return render(request, 'pata_gift/index.html', context_dic)
